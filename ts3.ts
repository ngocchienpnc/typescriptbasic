
let anotherBin: number = 0b010;
let octal: number = 0o10;
let hexadecimal: number = 0XA;
let big: bigint = 9007199254740991n;

let number: string = `anotherBin: ${anotherBin}
octal: ${octal}
hexadecimal: ${hexadecimal}
big: ${big}`

console.log(number);


//String
let firstName: string ='John';
let title: string = 'Web Developer'

let profile: string = `I'm ${firstName}. 
I'm a ${title}`
console.log(profile);


//Boolean
let pending: boolean = false;
let NotPending = !pending;

let hasError: boolean = false;
let competed: boolean = true;

let result1= hasError && competed

console.log(result1);

let result= pending || NotPending;


console.log(result);


//Object

let user: object;

user= {
    name: 'John',
    age: 36,
    title: 'Web Developer'
}
let employee: {
    firstName: string,
    lastName: string,
    age: number,
    jobTitle: string
}

employee= {
    firstName: 'John',
    lastName: 'D',
    age:20,
    jobTitle: 'Web Developer'
}

console.log(employee.age);
console.log(user);







