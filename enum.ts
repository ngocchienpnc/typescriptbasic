//Enum
enum Month{
    Jan,
    Feb,
    Mar,
    Apr,
    May,
    Jun,
    Jul,
    Aug,
    Sep,
    Oct,
    Nov,
    Dec
}

enum ApprovalStatus{
    draft,
    submitted,
    approved,
    rejected
}

function isItSummer(month: Month){
    let isSummer:boolean;

    switch (month) {
        case Month.Jun:
        case Month.Jul:
        case Month.Aug:

            isSummer = true;
            
            break;
    
        default:
            isSummer = false;
            break;
    }
    return isSummer;
}

const request  = {
    id: 1,
    status: ApprovalStatus.approved,
    description: 'Please approve this request'
};

if (request.status === ApprovalStatus.approved){
    console.log('Send email to the Applicant...');
}


console.log(isItSummer(5));
