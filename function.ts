function add1(a: any, b: any) {
    let c: any;
    if (typeof a === 'number' && typeof b === 'number') {
        if (a > b) {
            c = a - b
            console.log(`${a} - ${b} = ${c} `)
        } else if (a < b) {
            c = b - a
            console.log(`${b} - ${a} = ${c} `)

        }
        else c = a * b;
        console.log(`${a} * ${b} = ${c} `)

    }
    if (typeof a === 'string' && typeof b === 'string') {
        c = `a: ${a}
        b: ${b}`;
    }

}

console.log(add1(1, 2));
console.log(add1(2, 3));
console.log(add1(4, 4));

console.log(add1('mobile', 'web'));

//function types

let add2: (a: number, b: number) => number =
    function (x: number, y: number) {
        return x + y;
    };

console.log(add2(4, 4));

//TypeScript Optional Parameters
function multiply(a: number, b: number, c?: number): number {
    if (typeof c !== 'undefined') {
        return a * b * c;

    }
    return a * b;
}

console.log(multiply(4, 4));


//Default Parameters

function applyDiscount(price: number, discount: number = 0.05) {
    return price * (1 - discount);
}

console.log(applyDiscount(1000, 10));
console.log(applyDiscount(1000));


//Function Overloadings
function add3(a: number, b: number): number;
function add3(a: string, b: string): string;
function add3(a: any, b: any): any {
    return a + b;
}


function sum(a: number, b: number): number;
function sum(a: number, b: number, c: number): number;
function sum(a: number, b: number, c?: number): number {
    if (c) return a + b + c;
    return a + b;
}

class Counter {
    private current: number = 0;
    count(): number;
    count(target: number): number[];
    count(target?: number): number | number[] {
        if (target) {
            let values: number[] = [];
            for (let start: number = this.current; start <= target; start++) {
                values.push(start);
            }
            this.current = target;
            return values;
        }
        return ++this.current;
    }
    
}

let counter1 = new Counter();
console.log(counter1.count());
console.log(counter1.count(10));

///Rest Parameters

function getTotal(...number:number[]){
    let total = 0;
    number.forEach(num => total += num);
    return total;
}
console.log(getTotal(10,20,30,40));





