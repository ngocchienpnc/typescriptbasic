abstract class Person{
    private firstName: string;
    private lastName: string;
    private age: number;
    private address: string;
    readonly birthDate: Date;

    constructor(firstName: string, lastName: string, age: number, address: string,  birthDate: Date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
        this.birthDate = birthDate;
    }
    abstract getSalary(): number

    public getProfile(){
        return `Name: ${this.firstName} ${this.lastName}
        age: ${this.age}
        address: ${this.address}
        birthDate: ${this.birthDate}
        salary: ${this.getSalary}`
    }


}
class Person2 extends Person{
    constructor(firstName: string, lastName: string, age: number, address: string,  birthDate: Date,private salary: number){
        super(firstName, lastName, age, address, birthDate,);
    }
    getSalary(): number {
        return this.salary;
    }

}


// let person1 = new Person('Thiện', 'SV', 22, 'Hưng yên',new Date(2001,12,24));
let Thien= new Person2('Thiện', 'SV', 22, 'Hưng yên',new Date(2001,12,24),1200)
console.log(Thien.getProfile());

