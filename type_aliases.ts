type n = number;

let Age: n;

Age = 20;
console.log(Age);

type alphanumeric = number | string;

type o = object;
let input: alphanumeric;
input = 1;
input = 'abc'

console.log(input);

